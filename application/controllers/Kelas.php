<?php
    defined('BASEPATH') OR exit('No direct script access allowed');

    require APPPATH . '/libraries/REST_Controller.php';
    use Restserver\Libraries\REST_Controller;
    
    class Kelas extends REST_Controller {

        function __construct($config = 'rest') {
            parent::__construct($config);
            $this->load->database(); //optional
            $this->load->model('M_Kelas');
            $this->load->library('form_validation');
        }

        function index_get()
        {
            $id = $this->get('id');
            if ($id == '') {
                $data = $this->M_Kelas->fetch_all();
            } else {
                $data = $this->M_Kelas->fetch_single_data($id);
            }

            $this->response($data, 200);
        }

        function index_post()
        {
            if ($this->post('id') == '') {
                $response = array(
                    'status' => 'fail',
                    'field' => 'id',
                    'message' => 'Isian id tidak boleh kosong',
                    'status_code' => 502
                );

                return $this->response($response);
            }
            
            if ($this->post('kelas_code') == '') {
                $response = array(
                    'status' => 'fail',
                    'field' => 'kelas_code',
                    'message' => 'Isian kode tidak boleh kosong',
                    'status_code' => 502
                );

                return $this->response($response);
            }

            if ($this->post('nama_kelas') == '') {
                $response = array(
                    'status' => 'fail',
                    'field' => 'nama_kelas',
                    'message' => 'Isian nama kelas tidak boleh kosong',
                    'status_code' => 502
                );

                return $this->response($response);
            }
            $data = array(
                'id' => trim($this->post('id')),
                'class_code' => trim($this->post('kelas_code')),
                'class_name' => trim($this->post('nama_kelas')),
            );
            $this->M_Kelas->insert_api($data);
            $last_row = $this->db->select('*')->order_by('id',"desc")->limit(1)->get('kelas')->row();
            $response = array(
                'status' => 'success',
                'data' => $last_row,
                'status_code' => 201,
            );

            return $this->response($response);
        }

        function index_put()
        {
            $id = $this->put('id');
            $check = $this->M_Kelas->check_data($id);
            if ($check == false) {
                $error = array(
                    'status' => 'fail',
                    'field' => 'id',
                    'message' => 'Data tidak ditemukan',
                    'status_code' => 502
                );

                return $this->response($error);
            }
            if ($this->put('kelas_code') == '') {
                $response = array(
                    'status' => 'fail',
                    'field' => 'kelas_code',
                    'message' => 'Isian kode tidak boleh kosong',
                    'status_code' => 502
                );

                return $this->response($response);
            }
            if ($this->put('nama_kelas') == '') {
                $response = array(
                    'status' => 'fail',
                    'field' => 'nama_kelas',
                    'message' => 'Isian kelas tidak boleh kosong',
                    'status_code' => 502
                );

                return $this->response($response);
            }
            $data = array(
                'class_code' => trim($this->put('kelas_code')),
                'class_name' => trim($this->put('nama_kelas'))
            );

            $this->M_Kelas->update_data($id,$data);
            $newData = $this->M_Kelas->fetch_single_data($id);
            $response = array(
                'status' => 'success',
                'data' => $newData,
                'status_code' => 200,
            );

            return $this->response($response);
        }

        function index_delete() {
            $id = $this->delete('id');
            $check = $this->M_Kelas->check_data($id);
            if ($check == false) {
                $error = array(
                    'status' => 'fail',
                    'field' => 'id',
                    'message' => 'Data tidak ditemukan',
                    'status_code' => 502
                );

                return $this->response($error);
            }
            $delete = $this->M_Kelas->delete_data($id);
            $response = array(
                'status' => 'success',
                'data' => null,
                'status_code' => 200,
            );

            return $this->response($response);

        }
    }
?>