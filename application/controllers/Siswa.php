<?php
    defined('BASEPATH') OR exit('No direct script access allowed');

    require APPPATH . '/libraries/REST_Controller.php';
    use Restserver\Libraries\REST_Controller;
    
    class Siswa extends REST_Controller {

        function __construct($config = 'rest') {
            parent::__construct($config);
            $this->load->database(); //optional
            $this->load->model('M_Siswa');
            $this->load->library('form_validation');
        }

        function index_get()
        {
            $id = $this->get('id');
            if ($id == '') {
                $data = $this->M_Siswa->fetch_all();
            } else {
                $data = $this->M_Siswa->fetch_single_data($id);
            }

            $this->response($data, 200);
        }

        function index_post()
        {
            if ($this->post('id') == '') {
                $response = array(
                    'status' => 'fail',
                    'field' => 'id',
                    'message' => 'Isian id tidak boleh kosong',
                    'status_code' => 502
                );

                return $this->response($response);
            }
            
            if ($this->post('nama') == '') {
                $response = array(
                    'status' => 'fail',
                    'field' => 'nama',
                    'message' => 'Isian nama tidak boleh kosong',
                    'status_code' => 502
                );

                return $this->response($response);
            }

            if ($this->post('id_class') == '') {
                $response = array(
                    'status' => 'fail',
                    'field' => 'id_class',
                    'message' => 'Isian id class tidak boleh kosong',
                    'status_code' => 502
                );

                return $this->response($response);
            }

            if ($this->post('tanggal_lahir') == '') {
                $response = array(
                    'status' => 'fail',
                    'field' => 'tanggal_lahir',
                    'message' => 'Isian tanggal lahir kelas nama tidak boleh kosong',
                    'status_code' => 502
                );

                return $this->response($response);
            }

            if ($this->post('gender') == '') {
                $response = array(
                    'status' => 'fail',
                    'field' => 'gender',
                    'message' => 'Isian gender kelas nama tidak boleh kosong',
                    'status_code' => 502
                );

                return $this->response($response);
            }

            if ($this->post('address') == '') {
                $response = array(
                    'status' => 'fail',
                    'field' => 'address',
                    'message' => 'Isian address tidak boleh kosong',
                    'status_code' => 502
                );

                return $this->response($response);
            }

            $data = array(
                'id' => trim($this->post('id')),
                'name' => trim($this->post('nama')),
                'class_id' => trim($this->post('id_class')),
                'date_birth' => trim($this->post('tanggal_lahir')),
                'gender' => trim($this->post('gender')),
                'address' => trim($this->post('address'))
            );
            $this->M_Siswa->insert_api($data);
            $last_row = $this->db->select('*')->order_by('id',"desc")->limit(1)->get('siswa')->row();
            $response = array(
                'status' => 'success',
                'data' => $last_row,
                'status_code' => 201,
            );

            return $this->response($response);
        }

        function index_put()
        {
            $id = $this->put('id');
            $check = $this->M_Siswa->check_data($id);
            if ($check == false) {
                $error = array(
                    'status' => 'fail',
                    'field' => 'id',
                    'message' => 'Data tidak ditemukan',
                    'status_code' => 502
                );

                return $this->response($error);
            }
            if ($this->put('nama') == '') {
                $response = array(
                    'status' => 'fail',
                    'field' => 'nama',
                    'message' => 'Isian nama tidak boleh kosong',
                    'status_code' => 502
                );

                return $this->response($response);
            }
            if ($this->put('id_class') == '') {
                $response = array(
                    'status' => 'fail',
                    'field' => 'id_class',
                    'message' => 'Isian id class tidak boleh kosong',
                    'status_code' => 502
                );

                return $this->response($response);
            }

            if ($this->put('tanggal_lahir') == '') {
                $response = array(
                    'status' => 'fail',
                    'field' => 'tanggal_lahir',
                    'message' => 'Isian tanggal lahir tidak boleh kosong',
                    'status_code' => 502
                );

                return $this->response($response);
            }

            if ($this->put('gender') == '') {
                $response = array(
                    'status' => 'fail',
                    'field' => 'gender',
                    'message' => 'Isian gender tidak boleh kosong',
                    'status_code' => 502
                );

                return $this->response($response);
            }

            if ($this->put('address') == '') {
                $response = array(
                    'status' => 'fail',
                    'field' => 'address',
                    'message' => 'Isian address tidak boleh kosong',
                    'status_code' => 502
                );

                return $this->response($response);
            }

            $data = array(
                'id' => trim($this->put('id')),
                'name' => trim($this->put('nama')),
                'class_id' => trim($this->put('id_class')),
                'date_birth' => trim($this->put('tanggal_lahir')),
                'gender' => trim($this->put('gender')),
                'address' => trim($this->put('address'))
            );

            $this->M_Siswa->update_data($id,$data);
            $newData = $this->M_Siswa->fetch_single_data($id);
            $response = array(
                'status' => 'success',
                'data' => $newData,
                'status_code' => 200,
            );

            return $this->response($response);
        }

        function index_delete() {
            $id = $this->delete('id');
            $check = $this->M_Siswa->check_data($id);
            if ($check == false) {
                $error = array(
                    'status' => 'fail',
                    'field' => 'id',
                    'message' => 'Data tidak ditemukan',
                    'status_code' => 502
                );

                return $this->response($error);
            }
            $delete = $this->M_Siswa->delete_data($id);
            $response = array(
                'status' => 'success',
                'data' => null,
                'status_code' => 200,
            );

            return $this->response($response);

        }

    }
?>